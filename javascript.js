//Aliases 
window.onload = run;

function run() {

    drawUnicodeSymbols();
}

let Application = PIXI.Application,
    Text = PIXI.Text,
    TextStyle = PIXI.TextStyle;
//settings
let rows = 8,
    cols = 8,
    offset = 40,
    unicodeSym = ["◊", "✚", '♥', '▲'],
    toCheck = [], // будет хранить точки которые надо проверить
    similarElem = []; //  элементы одной групы
/*
при желании добавить символ
unicodeSym.push("sym1","sym2");
*/
let app = new Application({
    width: cols * offset,
    height: rows * offset,
    backgroundColor: 0xF7ECE4
});
document.body.appendChild(app.view);
//
let style = new TextStyle({
    fontFamily: "Arial",
    fill: "red",

});
//
function createUnicodeSymbols(_text, tag, x, y, anchor) { //
    this._text = _text;
    this._text.tag = tag; //По тегу будут сравниваться элементы
    this._text.x = x;
    this._text.y = y;
    this._text.anchor.set(anchor);
    this._text.interactive = true;
    this._text.buttonMode = true;
}
createUnicodeSymbols.prototype.check = function (sym2) {

    return this._text.tag === sym2._text.tag &&
        similarElem.indexOf(sym2) === -1;
};
//Возвращает преобразованную позицию в двухмерные координаты елемента который был нажат 
function getPositionInMatrix(element, array) {

    let eventIndex = Array.prototype.indexOf.call(array, element),
        position = {};
    position.row = 0;
    position.col = 0;

    if (eventIndex > cols - 1) {
        position.col = eventIndex % cols;
        position.row = Math.floor(eventIndex / rows);
    } else {
        position.col = eventIndex;
    }
    console.log(position);
    return position;
}

function getRandVal(from, to) {

    return Math.floor(Math.random() * (to - from) + from);
}
let elemArr = [], // матрица rows * cols
    oneArr = []; // массив всех юникод символов

function drawUnicodeSymbols() {
    for (let i = 0; i < rows; i += 1) {

        let tmp = [];

        for (let j = 0; j < cols; j += 1) {
            //set x and y of text
            let x = j * offset + Math.round((offset / 2)),
                y = i * offset + Math.round((offset / 2)),
                //Эта переменная randNumber нужна чтобы связать символ с тегом
                randNumber = getRandVal(0, unicodeSym.length);

            let unicode = new createUnicodeSymbols(new Text(unicodeSym[randNumber]),
                randNumber, x, y, 0.5);

            //заполнение массивов и отрисовка 
            tmp.push(unicode);
            oneArr.push(unicode._text)
            app.stage.addChild(unicode._text);
            //get position in matrix elemArr
            unicode._text.on("click", event => {
                positionIs = getPositionInMatrix(event.target, oneArr);
                checkSame(positionIs.row, positionIs.col)
            });
        }
        elemArr.push(tmp);
    }

}

function checkSame(row, col) {
    if (similarElem.length) similarElem = [];
    toCheck.push(new Array(elemArr[row][col], row, col));
    do {
        let len = 0;
        toCheck.forEach(cur => {
            //right
            if (cur[2] < cols - 1 &&
                cur[0].check(elemArr[cur[1]][cur[2] + 1])) {

                let right = elemArr[cur[1]][cur[2] + 1];
                similarElem.push(right);
                toCheck.push(new Array(right, cur[1], cur[2] + 1));
            }
            //left
            if (cur[2] !== 0 &&
                cur[0].check(elemArr[cur[1]][cur[2] - 1])) {

                let left = elemArr[cur[1]][cur[2] - 1];
                similarElem.push(left);
                toCheck.push(new Array(left, cur[1], cur[2] - 1));
            }
            //bottom
            if (cur[1] !== 0 &&
                cur[0].check(elemArr[cur[1] - 1][cur[2]])) {

                let bot = elemArr[cur[1] - 1][cur[2]];
                similarElem.push(bot);
                toCheck.push(new Array(bot, cur[1] - 1, cur[2]));
            }
            //top
            if (cur[1] < rows - 1 &&
                cur[0].check(elemArr[cur[1] + 1][cur[2]])) {

                let top = elemArr[cur[1] + 1][cur[2]];
                similarElem.push(top);
                toCheck.push(new Array(top, cur[1] + 1, cur[2]));
            }
            len += 1;
        });
        toCheck.splice(0, len);
    }
    while (toCheck.length)
    if (similarElem.length) giveNewColor(similarElem);
}

function giveNewColor(array) {
    array.forEach(e => {

        e._text.style = style;
        e._text.scale.set(1.2);
    });
    setTimeout(changeSym, 250);

    function changeSym() {
        array.forEach(current => {

            let x = current._text.x,
                y = current._text.y;

            randNumber = getRandVal(0, unicodeSym.length);

            let unicode = new createUnicodeSymbols(
                new Text(unicodeSym[randNumber]),
                randNumber, x, y, 0.5);

            current._text.text = unicode._text.text;
            current._text.style = null;
            current._text.tag = randNumber;
            current._text.scale.set(1);
        });
    }

}