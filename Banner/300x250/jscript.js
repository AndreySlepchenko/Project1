let app = new PIXI.Application(300, 250, {
        antialias: true
    }),
    run = function() {
        document.body.appendChild(app.view);
        drawPrimitives();
    };

//Aliases
let stage = app.stage,
    Graphics = PIXI.Graphics,
    Text = PIXI.Text,
    TextStyle = PIXI.TextStyle,
    Rectangle = PIXI.Rectangle;


//Text Styles
let style1 = new TextStyle({
        fontSize: 25,
        fontFamily: 'Sans-serif',
        fill: '#ffffff',
        letterSpacing: 9

    }),
    style2 = new TextStyle({
        fontSize: 18,
        fontFamily: "Arial",
        fill: "#72985A",
        align: "center"
    });

//settings to ticker
let offset = 50,
    speedTicker = 5,
    state = undefined,
    allOk = false;

// тут использовал прямоугольник чтоб на весь канвас повесить клик, думаю что это не лучшее решение

let rectForAll  = new Graphics();
rectForAll.beginFill(0x00000);
rectForAll.drawRect(0,0,app.view.width,app.view.height);
rectForAll.endFill();
rectForAll.interactive = true;
rectForAll.buttonMode = true;
rectForAll.on("click",onClick);

stage.addChild(rectForAll);

 

function drawPrimitives() {
    // create rect for Alconost
    let graphics = new Graphics(),
        gr = graphics;
    gr.beginFill(0x0075FF);
    // alcoпost rect
    gr.moveTo(35, 20);
    gr.lineTo(265, 20);
    gr.lineTo(265, 35);
    gr.lineTo(255, 35);
    gr.lineTo(255, 50);
    gr.lineTo(245, 50);
    gr.lineTo(245, 65);
    gr.lineTo(35, 65);
    gr.endFill();
    stage.addChild(gr);

    //Alconost text
    let text1 = new Text("alconost".toUpperCase(), style1);
    text1.x = 45;
    text1.y = 27;
    stage.addChild(text1);

    //Next text
    let text2 = new Text("Локализация приложений,\n игр, сайтов", style2);
    text2.x = 38;
    text2.y = 75;
    stage.addChild(text2);

    //wrap rectangle
    let rectangle = new Graphics();
    rectangle.lineStyle(1, 0x72985A, 1);
    rectangle.drawRect(0, 0, gr.width, 35);
    rectangle.x = 35;
    rectangle.y = 200;
    stage.addChild(rectangle);

    //text inside rect
    let doOrder = new Text("Заказать", style2);
    doOrder.anchor.set(0.5);
    doOrder.x = app.view.width / 2;
    doOrder.y = 217;
    stage.addChild(doOrder);
    //вручную заполнил(
    let textToTicker = ["68 языков", "тестирование локализации", "выделенный менеджер проектов 24/7",
        "переводчики-носители языка", "лингвистическое тестирование", "память переводов, глоссарии",
        "непрерывная локализация", "доперевод обновлений", "облачная платформа",
        "API интерфей командной строки", "любые форматы строковых ресурсов", "локализация IOS-приложений",
        "локализация Androind-приложений"

    ];

    if (textToTicker.length) createTicker(textToTicker); 
}
//create mask for ticker
let mask = new Graphics();
mask.drawRect(35, 0, 230, app.view.height);
stage.addChild(mask);
// tickerLen stores the widht of all strings 
let tickerLen = 0,
    tmp = []; // this array storing all text objects that created  by constructor
function createTicker(textToRun) {

    let renderTexture = PIXI.RenderTexture.create(app.view.width, app.view.height);
    for (let i = 0; i < textToRun.length; i += 1) {

        let str = new Text(textToRun[i], {
            fill: "#ffffff",
            fontSize: 30
        });
        tickerLen += str.width;
        i === 0 ? str.x = app.view.width + str.width :
            str.x = tmp[i - 1].x + str.width + offset;
        str.y = 137;
        str.anchor.set(1, 0);
        tmp.push(str);

        stage.addChild(str);
        str.mask = mask;
        app.renderer.render(str, renderTexture);
    }
    allOk = true;
}
//ticker status
state = play;
app.ticker.add(delta => {
    if (allOk) state();
});
// 
function play() {

    tmp[0].x -= speedTicker; // принципом змейки в некотором смысле

    for (let i = 1; i < tmp.length; i += 1) {

        tmp[i].x = tmp[i - 1].x + tmp[i].width + offset;
    }
    if (tmp[0].x < -tickerLen - app.view.width - tmp[0].width) {
        tmp[0].x = app.view.width + tmp[0].width;
    }
}

function onClick(event) {
    alert("say  hello");
    //window.open('https://google.com');
}
window.onload = run;